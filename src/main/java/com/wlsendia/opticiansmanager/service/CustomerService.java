package com.wlsendia.opticiansmanager.service;

import com.wlsendia.opticiansmanager.entity.Customer;
import com.wlsendia.opticiansmanager.model.CustomerItem;
import com.wlsendia.opticiansmanager.model.CustomerRequest;
import com.wlsendia.opticiansmanager.model.CustomerResponse;
import com.wlsendia.opticiansmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

//@Service
@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request){
        Customer addData = new Customer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setJoinDate(LocalDate.now());
        addData.setLeftVision(request.getLeftVision());
        addData.setRightVision(request.getRightVision());

        customerRepository.save(addData);
    }

    public List<CustomerItem> getCustomers(){
        List<CustomerItem> result = new LinkedList<>();

        List<Customer> originList = customerRepository.findAll();

        for (Customer item : originList)/*이름 전화번호 시력 가입*/{
            CustomerItem temp = new CustomerItem();
            temp.setCustomerName(item.getCustomerName());
            temp.setCustomerPhone(item.getCustomerPhone());
            temp.setJoinDate(item.getJoinDate());
            temp.setVision(item.getLeftVision() + "/" + item.getLeftVision());

            result.add(temp);
        }
        return result;
    }

    public CustomerResponse getCustomer(long id){
        CustomerResponse result = new CustomerResponse();

        Customer originData = customerRepository.findById(id).orElseThrow();

        result.setId(id);
        result.setCustomerName(originData.getCustomerName());
        result.setCustomerPhone(originData.getCustomerPhone());
        result.setJoinDate(originData.getJoinDate());
        result.setVision(originData.getLeftVision() + "/" + originData.getRightVision());
        result.setIsgood(
                (((originData.getLeftVision() > 2.0) && (originData.getRightVision() > 2.0)) ? true : false)
        );




        return result;
    }

}
