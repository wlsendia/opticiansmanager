package com.wlsendia.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerItem {

    // 전체 고객 출력

    private String customerName;

    private String customerPhone;

    private LocalDate joinDate;

    private String vision;
}
