package com.wlsendia.opticiansmanager.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerRequest {

//컨트롤러 -> 서비스 -> 레포지토리 -> 엔티티
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 7, max = 20)
    private String customerPhone;

    @NotNull
    private Float leftVision;


    @NotNull
    private Float rightVision;

}
