package com.wlsendia.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerResponse {

    // 특정 고객 출력
    private Long id;
    private String customerName;
    private String customerPhone;
    private LocalDate joinDate;
    private String vision;

    private Boolean isgood;


}
