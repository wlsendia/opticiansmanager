package com.wlsendia.opticiansmanager.controller;

import com.wlsendia.opticiansmanager.model.CustomerItem;
import com.wlsendia.opticiansmanager.model.CustomerRequest;
import com.wlsendia.opticiansmanager.model.CustomerResponse;
import com.wlsendia.opticiansmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")//고객 정보 입력
    public String setCustomer(@RequestBody @Valid CustomerRequest request){
        customerService.setCustomer(request);
        return "OK";
    }
    @GetMapping("/people")//전체 고객 목록
    public List<CustomerItem> getCustomers(){
        List<CustomerItem> result = new LinkedList<>();
        result = customerService.getCustomers();

        return result;
    }

    @GetMapping("/data/id/{id}")//특정 고객 목록
    public CustomerResponse getCustomer(@PathVariable long id) {
        CustomerResponse result = customerService.getCustomer(id);

        return result;
    }
}
// 1dnjf 18dlf